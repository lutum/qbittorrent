FROM alpine:3.16

RUN apk add --no-cache qbittorrent-nox

COPY qBittorrent.conf /config/qBittorrent/qBittorrent.conf
COPY watched_folders.json /config/qBittorrent/watched_folders.json

ENTRYPOINT ["qbittorrent-nox", "--profile=/config"]
